/**
 * @BASE_URL   String   请求地址
 */

let BASE_URL = ""

if (process.env.NODE_ENV === 'development') {
	// 开发环境
	// BASE_URL = 'http://192.168.101.30:9001'
	BASE_URL = 'http://img-code.natapp1.cc/stage-api'
} else {
	// 生产环境
	BASE_URL = ''
}

export default BASE_URL
