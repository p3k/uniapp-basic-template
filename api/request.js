import BASE_URL from './config.js'
import i18n from '@/locale/index.js'

/** 
 * @url     String   请求链接
 * @config  Object   请求参数
 */

export const request = async (url, config = {}) => {
	return new Promise(async (resolve, reject) => {
		console.log('🟩 ~ file: request.js ~ line 11:', config);
		await uni.getNetworkType({
			async complete(res) {
				// networkType: wifi 2g 3g 4g 5g ethernet(有线网络) unknown(Android下不常见的网络类型) none(无网络)
				if (res.networkType === 'none') {
					uni.showToast({
						title: i18n.t('common.noNetwork'),
						icon: 'none',
						duration: 3000
					})
					reject({errMsg: i18n.t('common.noNetwork')})
				} else {
					// resolve(networkRequest(url, config))
					let token = uni.getStorageSync("token") || ''
					await uni.request({
						url: BASE_URL + url,
						data: config.data || '',
						header: {
							'content-type': 'application/json;charset=utf-8',
							'Authorization': 'Bearer ' + token
						},
						method: config.method || 'POST',
						complete(res) {
							// 请求超时  request:fail timeout:浏览器提示信息  request:fail abort statusCode:-1 timeout: APP提示信息
							if (res.errMsg === 'request:fail timeout' || res.errMsg === 'request:fail abort statusCode:-1 timeout') {
								uni.showToast({
									title: i18n.t('common.timeout'),
									icon: 'none'
								})
								// reject(new Error(res.errMsg))
								reject({errmsg: i18n.t('common.timeout')})
							} else if (res.data.code === 200) {
								console.log('🉐 ~ file: request.js ~ line 45:', res);
								resolve(res.data)
							} else if (res.data.code === 401) {
								// 获取错误信息
								uni.$showModal({
									title: i18n.t('common.tokenError'),
									icon: 'none',
									showCancel: false,
									complete(res) {
										uni.reLaunch({
											url: '/pages/login/index'
										})
										reject(res.data.msg)
									}
								})
								console.log('🉐🉐 error ~ file: request.js ~ line 58:', res);
								reject(res.data.msg)
							} else {
								// 获取错误信息
								uni.showToast({
									title: res.data.msg || res.data.Forbidden || res.data.error || i18n.t('common.title'),
									icon: 'none',
								})
								console.log('🉐🉐 error ~ file: request.js ~ line 66:', res);
								// reject(new Error(res.data.msg))
								// reject(res.data)
								reject(res.data.msg)
								// uni.reLaunch({
								// 	url: '/pages/detail/index'
								// })
							}
						}
					})
				}
			}
		})
	})
}
