import { request } from './request';
import { driverHomeApi } from './driver/home.js'
import { driverTakeApi } from './driver/take.js'
import { commonApi } from './common/index.js'

const apiObject = {
	...commonApi,
	...driverHomeApi,
	...driverTakeApi
}

const API = Object.keys(apiObject).reduce((acc, curr) => {
	return {
		...acc,
		[curr]: (input) => request(apiObject[curr].url, input),
	}
}, {})
export { API }