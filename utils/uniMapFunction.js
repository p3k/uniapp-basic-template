import i18n from '@/locale/index.js'
import config from '@/api/config.js'

/**
 * @desc  		 弹窗
 * @options    Object			请求对象数据
 * @title      String     标题
 * @content    String     内容
 * @showCancel Boolean    是否显示取消按钮
 * @success    Function   确定的回调
 * @cancel     Function   取消的回调
 */
const showModal = (options = {}) => {
	uni.showModal({
		title: options.title || i18n.t('common.title'),
		content: options.content || i18n.t('common.content'),
		showCancel: !!options.showCancel,
		success(res) {
			if (res.confirm) {
				options.success && options.success()
			} else {
				options.cancel && options.cancel()
			}
		}
	})
}

/**
 * @desc    是否禁用下拉加载
 * @value   Boolean   是/否禁用下拉刷新
 */
const pullDownRefresh = (value) => {
	const pages = getCurrentPages();
	const page = pages[pages.length - 1];  
	const currentWebview = page.$getAppWebview();
	currentWebview.setStyle({
	  pullToRefresh: {
	    support: value,
	    style: plus.os.name === 'Android' ? 'circle' : 'default'
	  }
	});
}


/**
 * @desc        图片上传
 * @options     Object     请求对象数据
 * @url					String		 图片地址
 * @success     Function   成功的回调
 * @fail				Function   失败的回调
 */
const uploadFile = (options = {}) => {
	uni.uploadFile({
		url: config.BASE_URL + '/common/upload',
		filePath: options.url,
		name: 'file',
		success(res) {
			options.success && options.success()
		},
		fail(err) {
			options.fail && options.fail()
		}
	})
}

export {
	showModal,
	pullDownRefresh
}
