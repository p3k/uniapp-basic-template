import Vue from 'vue'
import VueI18n from 'vue-i18n'
import zh from './zh-Hans.json'
import en from './en.json'
Vue.use(VueI18n)
let i18nConfig = {
  locale: uni.getLocale(),
	messages: {
		'zh-Hans': zh,
		en
	},
	silentTranslationWarn: true,
}

const i18n = new VueI18n(i18nConfig)
export default i18n