
import { mapGetters, mapMutations } from 'vuex'
export default {
  install(Vue) {
    Vue.mixin({
      data() {
        return {}
      },
      methods: {
        ...mapMutations(['setCurThemeType'])
      },
      computed: {
        ...mapGetters(['themeType'])
      }
    })
  }
}
