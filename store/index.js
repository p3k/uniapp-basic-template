import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import consignor from './modules/consignor/consignor.js'
import driver from './modules/driver/driver.js'
import i18n from '@/locale/index.js'
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
		themeType: uni.getStorageSync('theme') || 'light',
		status: 'operation'
	},
  mutations: {
		// tabs切换
		selectStatus(state, status) {
		  state.status = status
		},
		setCurThemeType(state, data) {
			state.themeType = data;
		}
	},
  actions: {},
  modules: {
    consignor,
    driver,
  },
  getters: {
		...getters,
		status: (state) => state.status,
		themeType: (state) => state.themeType,
		tabBarList: (state) => {
			return {
				operation: [
					{
						title: i18n.t('tab.home'),
						icon: 'home',
						pageName: 'driverHome'
					},
					{
						title: i18n.t('tab.take'),
						icon: 'car',
						pageName: 'driverTake'
					},
					{
						title: i18n.t('tab.user'),
						icon: 'account',
						pageName: 'driverUser'
					}
				],
				carrier: [
					{
						title: i18n.t('tab.home'),
						icon: 'home',
						pageName: 'driverHome'
					},
					{
						title: i18n.t('tab.take'),
						icon: 'car',
						pageName: 'driverTake'
					},
					{
						title: i18n.t('tab.user'),
						icon: 'account',
						pageName: 'driverUser'
					}
				],
				consignor: [
					{
						title: i18n.t('tab.home'),
						icon: 'home',
						pageName: 'consignorHome'
					},
					{
						title: i18n.t('tab.send'),
						icon: 'car',
						pageName: 'consignorSend'
					},
					{
						title: i18n.t('tab.user'),
						icon: 'account',
						pageName: 'consignorUser'
					}
				]
			}[state.status]
		}
	},
})

export default store
