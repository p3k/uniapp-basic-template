const state = {
  location: {}
}
const mutations = {
  setLocation(state, data) {
		state.location = data
	}
}
const actions = {}
const getters = {}
export default {
  // 开启命名空间
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
