import Vue from 'vue'
import App from './App'
import store from './store'
import i18n from './locale/index.js'
import mixins from './mixins/mixins.js'
import uView from '@/uni_modules/uview-ui'
import { API } from '@/api/api.js'
Vue.use(mixins)
Vue.use(uView)
// 请求 api
Vue.prototype.$api = API
import {
	showModal,
	pullDownRefresh,
} from './utils/uniMapFunction.js'
Vue.prototype.$showModal = showModal
Vue.prototype.$pullDownRefresh = pullDownRefresh

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
		i18n,
		store,
    ...App,
})
app.$mount()
